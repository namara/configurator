# Configurator

Quickly turn an dotevn environment style file to etcd keys

## Usage

```bash
$ ./configurator <envfile> <namespace>
$ # Example
$ ./configurator env /somenamepace
$ # If env contains "VAR=one" this will call "etcdctl set /somenamespace/VAR one"
```
