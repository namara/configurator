#!/bin/bash

source ~/git/golang-crosscompile/crosscompile.bash

go build -o build/configurator configurator.go
go-linux-amd64 build -o build/configurator-linux_amd64 configurator.go