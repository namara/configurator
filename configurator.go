package main
 
import (
    "fmt"
    "io/ioutil"
    "os"
    "os/exec"
    "strings"
)

func check(e error) {
    if e != nil {
        panic(e)
    }
}

func setKey(namespace string, key string, value string) {
  key = namespace + "/" + key
  fmt.Printf("Setting %s=%s\n", key, value)
  cmd := exec.Command("consul-kv", "set", key, value)
  cmd.Run()
}

func deleteKey(namespace string, key string) {
  key = namespace + "/" + key
  fmt.Printf("Deleting %s\n", key)
  cmd := exec.Command("consul-kv", "del", key)
  cmd.Run()
}

func extractKeyValue(line string) (key string, value string) {
  var kv []string

  if strings.Contains(line, "=") {
    kv = strings.SplitN(line, "=", 2)

    return strings.TrimSpace(kv[0]), strings.TrimSpace(kv[1])
  }

  return
}

func extractKnownKeys(namespace string) (known_keys []string) {
  out, _ := exec.Command("consul-kv", "keys", namespace).Output()
  lines := strings.Split(string(out), "\n")

  for _, line := range lines {
    if len(line) > 0 {
      key := strings.TrimPrefix(line, namespace + "/")
      known_keys = append(known_keys, key)
    }
  }

  return
}

func containsValue(arr []string, value string) bool {
  for _, a := range arr { if a == value { return true } }
  return false
}

func deleteExtraKeys(namespace string, old_keys []string, new_keys []string) {
  for _, key := range old_keys {
    if !containsValue(new_keys, key) {
      deleteKey(namespace, key)
    }
  }
}

func main() {
  config_file := os.Args[1]
  var namespace string
  if len(os.Args) > 2 {
    namespace = os.Args[2]
  }

  old_keys := extractKnownKeys(namespace)

  dat, err := ioutil.ReadFile(config_file)
  check(err)
  lines := strings.Split(string(dat), "\n")

  var new_keys []string  
  var key, value string
  for _, line := range lines {
      key, value = extractKeyValue(line)
      if len(key) > 0 {
        new_keys = append(new_keys, key)
        setKey(namespace, key, value)
      }
  }

  deleteExtraKeys(namespace, old_keys, new_keys)
}